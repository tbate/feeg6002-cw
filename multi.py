# -*- coding: utf-8 -*-
import numpy as np
import scipy as sp
import scipy.linalg
import matplotlib.pyplot as plt
import math as m

"""QUESTION TOOLS"""

def embed(U):
    """Embed the array U giving the solutions at the inner nodes in
    the domain into a larger array including the boundary values
    """
    N = U.shape[0] + 2
    Ufull = np.zeros((N,N))
    Ufull[1:N-1, 1:N-1] = U
    return Ufull
    
def get_A(n):
    """Return matrix A for 2D Laplace equation using block diagonal
    structure, given the number of unknowns ’n’ in each direction.
    """
    Bdiag = -4 * np.eye(n)
    Bupper = np.diag([1] * (n - 1), 1)
    Blower = np.diag([1] * (n - 1), -1)
    B = Bdiag + Bupper + Blower
    # Creat a list [B,B,B,...,B] with n Bs
    blst = [B] * n
    # Unpack the list of diagonal blocks ’blst’
    # since block_diag expects each block to be passed as separate
    # arguments. This is the same as doing block_diag(B,B,B,...,B)
    A = sp.linalg.block_diag(*blst)
    # Upper diagonal array offset by n: we’ve got (n-1) I blocks
    # each containing n ones
    Dupper = np.diag(np.ones(n * (n - 1)), n)
    # Lower diagonal array offset by -n
    Dlower = np.diag(np.ones(n * (n - 1)), -n)
    A += Dupper + Dlower
    return A
    
def get_b(n):
    """Return column vector of size n^2 containing rho=2 where (x,y) = (0.5,0.5) and zero at all 
    boundaries."""
    b = np.zeros(n**2)
    #Set laplacian of u at x(0.5,0.5) = 2.0
    b[n**2/2] = 2.0
    return b
    
def iterate(A, b, x, omega=1.0):
    """Return new and improved x column vector for SOR iterations"""
    #produce relevant matrices
    d = np.diag(A)
    D = np.diagflat(d)
    L = np.tril(A) - D
    U = np.triu(A) - D
    Dinv = np.linalg.inv(D)
    D = Dinv[0,0]
    #compute new x
    for i in xrange(len(x)):
        x[i] = (omega*D)*(b[i] - np.dot(U[i], x) - np.dot(L[i], x)) + (1.0-omega)*x[i]
    return x
    
def plot_omegavsiter(N=23, tol=1.0e-9):
    """Plot omega vs iterations for SOR or Red-black gauss-seidel methods, 
    user must implement specific method within function"""
    omega = np.linspace(1.7,1.8,20)
    
    n = N - 2
    A = get_A(n)
    b = get_b(n)
    iterlst = []
    for w in omega:
        x = np.zeros(n**2)
        for i in xrange(1,1001):
            xold = x.copy()
            #implement method here (comment out un-used method)
            #x = iterate(A,b,x,w)
            x = iterate_rb(A, b, x, w, red=True)
            x = iterate_rb(A, b, x, w, red=False)
            dx = m.sqrt(np.dot((x - xold).flatten() ,x - xold))
            if dx < tol:
                iterlst.append(i)
                break
    plt.figure(1)
    plt.plot(omega, iterlst, 'o-', color='b')
    plt.xlabel('Omega')
    plt.ylabel('Iterations')
    plt.title("SOR Red-Black Omega vs Iterations: N = %d" % (N))
    
def get_A4(n):
    """Returns block diagonal A matrix for Ax = b for fourth order central difference approach"""
    A = np.zeros((n**2,n**2))
    #iterate through points in A and apply stencil
    for i in xrange(n):
        for j in xrange(n):
            S = np.zeros((n,n))
            if i + 2 > n-1:
                pass
            else:
                S[i+2,j] = -1.0
            if i + 1 > n-1:
                pass
            else:
                S[i+1,j] = 16.0            
            if j - 2 < 0:
                pass
            else:
                S[i,j-2] = -1.0               
            if j - 1 < 0:
                pass
            else:
                S[i,j-1] = 16.0                
            if j + 1 > n-1:
                pass
            else:
                S[i,j+1] = 16    
            if j + 2 > n-1:
                pass
            else:
                S[i,j+2] = -1.0           
            if i - 1 < 0:
                pass
            else:
                S[i-1,j] = 16               
            if i - 2 < 0:
                pass
            else:
                S[i-2,j] = -1.0
            S[i,j] = -60         
            s = S.flatten()
            k = i*n + j
            A[k] += s
    return A/12

def iterate_rb(A, b, x, omega=1.0, red=True):
    """Return new and improved x for guass-seidel red-black method"""
    #produce relevant matrices
    d = np.diag(A)
    D = np.diagflat(d) 
    L = np.tril(A) - D
    U = np.triu(A) - D
    n = int(m.sqrt(len(x)))
    #compute new x
    if red: #solve red points
        for i in xrange(n):
            for j in xrange(n):
                if (i+j) % 2==0 or i+j == 0: #even
                    ui = i*n + j
                    x[ui] = (omega*(1.0/D[ui,ui]))*(b[ui] - np.dot(U[ui], x) - np.dot(L[ui], x)) + (1.0-omega)*x[ui]
    else: #solve black points
        for i in xrange(n):
            for j in xrange(n):
                if (i+j) % 2 > 0: #odd
                    ui = i*n + j
                    x[ui] = (omega*(1.0/D[ui,ui]))*(b[ui] - np.dot(U[ui], x) - np.dot(L[ui], x)) + (1.0-omega)*x[ui]
    return x
    
def check(U, A, b, Q):
    """Check solutions for the laplacian rho = 2 at (x,y) = (0.5,0.5)"""
    c = np.dot(A,U)
    print "Q%d: The Laplacian rho = %.1f at (x,y) = (0.5, 0.5)" % (Q, c[U.shape[0]/2])
    
def sol_plot(Ufull):
    """plot solution on 2d grid using pcolor"""
    N = Ufull.shape[0]
    x = y = np.linspace(0, 1, N+1)
    X, Y = np.meshgrid(x,y)
    plt.figure(9)
    plt.clf()
    plt.pcolor(X, Y, Ufull)
    plt.axis('scaled')
    plt.colorbar()
    plt.xlabel('x (m)')
    plt.ylabel('y (m)')
    plt.title('u(x,y) on %dx%d grid' % (N,N))

    
"""QUESTION MAINS"""

def Q1(N=23):
    """Solve laplacian using finite central difference approach"""
    #Check N is odd
    if (N % 2==0):
        print "N must be an odd integer, solution will proceed with N=%d" % (N-1)
        N -= 1
    #Solve
    n = N - 2
    A = get_A(n)
    b = get_b(n)
    u = sp.linalg.solve(A, b)
    check(u,A,b,1)
    
   

def Q2(N=23, tol=1.0e-9, plot_err=False):
    """Produce solution x of Ax = b using gauss-seidel successive over-relaxation method"""
    omega = 1.758
    #Check N is odd
    if (N % 2==0):
        print "N must be an odd integer, solution will proceed with N=%d" % (N-1)
        N -= 1
    #To produce output x for check
    n = N - 2
    A = get_A(n)
    b = get_b(n)
    x = np.zeros(n**2)
    if plot_err == False:
        for i in xrange(1,1001):
            xold = x.copy()
            x = iterate(A, b, x, omega)
            dx = m.sqrt(np.dot((x - xold).flatten() ,x - xold))
            if dx < tol:
                break
        check(x,A,b,2)
    #To plot convergence
    if plot_err == True:
        iterlst = np.linspace(40,60,21,dtype='int16')
        errlst = []
        for i in iterlst:
            for j in xrange(1,i):
                xold = x.copy()
                x = iterate(A, b, x, omega)
                dx = m.sqrt(np.dot((x - xold).flatten() ,x - xold))
            errlst.append(dx)
        plt.figure(1)
        plt.semilogy(iterlst, errlst, 'o-', color='g', label="SOR")
        plt.xlabel('number of iterations')
        plt.ylabel('error')
        plt.title("SOR Convergence: N = %d, Omega = %.3f" % (N, omega) )
        plt.legend()
        
def Q3(N=23):
    """Solve Laplacian using a fourth order finite central difference stencil"""
    #Check N is odd
    if (N % 2==0):
        print "N must be an odd integer, solution will proceed with N=%d" % (N-1)
        N -= 1
    #produce elements of matrix equation
    n = N - 2
    A = get_A4(n)
    b = get_b(n)
    #solve
    u = sp.linalg.solve(A, b)
    check(u,A,b,3)
    
   

def Q4(N=23, tol=1.0e-9, plot_err=False):
    """Produce solution of Ax = b using red black gauss_seidel method"""
    omega = 1.758
    #Check N is odd
    if (N % 2==0):
        print "N must be an odd integer, solution will proceed with N=%d" % (N-1)
        N -= 1
    #Produce elements of matrix equation
    n = N - 2
    A = get_A(n)
    b = get_b(n)
    x = np.zeros(n**2)
    #To produce output x for check
    if plot_err == False:
        for i in xrange(1,1001):
            xold = x.copy()
            x = iterate_rb(A, b, x, omega, red=True)
            x = iterate_rb(A, b, x, omega, red=False)
            dx = m.sqrt(np.dot((x - xold).flatten() ,x - xold))
            if dx < tol:
                break
        check(x,A,b,4)
    #To plot convergence
    if plot_err == True:
        iterlst = np.linspace(40,60,21, dtype='int16')
        errlst = []
        for i in iterlst:
            for j in xrange(i):
                xold = x.copy()
                x = iterate_rb(A, b, x, omega, red=True)
                x = iterate_rb(A, b, x, omega, red=False)
                dx = m.sqrt(np.dot((x - xold).flatten() ,x - xold))
            errlst.append(dx)
        plt.figure(1)
        plt.semilogy(iterlst, errlst, 'o-', color='b', label="SOR_Red-Black")
        plt.xlabel('number of iterations')
        plt.ylabel('error')
        plt.title("SOR & Red-Black Convergence: N = %d, Omega = %.3f" % (N, omega) )
        plt.legend()
        

Q1(23)
Q2(23)
Q3(23)
Q4(23)


        
       
        
        
